# Course Goals App

Application developed in Section 2 of Udemy's "React Native - The Practical Guide" course by Maximilian Scharzmuller

## Course Notes

### Useful Resources & Links

- Official Docs: https://facebook.github.io/react-native/docs/getting-started

- Overview of available Components & APIs: https://facebook.github.io/react-native/docs/components-and-apis

- Expo Docs: https://docs.expo.io/versions/latest/
