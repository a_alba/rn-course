# React Native - The Practical Guide

Udemy Course by Maximilian Schwarzmuller about React Native framework to create native mobile apps that work on both Android and iOS using React and Javascript.

## Course outline

Multiple applications are going to developed from scratch to demonstrate and understand basic concepts, principles and uses of the library. This is a summary of the apps and the topics covered.

### Course goal app

Application developed from scratch in Section 2 that renders a list of goals entered by a modal user input. It demonstrates the use of basic React Native components, customs components and basic styling properties.
